#ifndef __INDEX_H__
#define __INDEX_H__


#define MULTILINE(...) #__VA_ARGS__

char *root=MULTILINE(
<html>
  <head>
    <meta charset="utf-8">
    <title>Crunch time 2019</title>
    <style>
      html,body {
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        background-color: #f0f0f0;
        font-family: Jazz LET, fantasy;
        overscroll-behavior: none;
      }
      canvas {
        display: block;
      }
      form {
        display: inline-block;
        background-color: #e0e0e0;
        border: 3px solid #444444;
        border-radius: 30px;
        padding: 30px;
      }
      div {
        padding: 10px;
        margin: auto;
        display: table;
        font-size: 30px;
      }
      input {
        font-size: 20px;
        margin: 10px;
      }
      input[type=submit]{
        padding: 10px;
        padding-left: 20px;
        padding-right: 20px;
      }
      h1 {
        padding: 10px;
        font-size: 40px;
        font-weight: normal;
      }
      body > div {
        margin: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
      }
    </style>
  </head>
  <body>
    <canvas id="canvas">
      le navigateur est trop vieux pour afficher l'application
    </canvas>
    <script>
      function setup(){
        var canvas=document.getElementById("canvas");
        var context=canvas.getContext("2d");
        var x=0.0,y=0.0,radius=50,minUpdateServerTime=50,connected=false;
        var websocket=new WebSocket("ws://"+location.host+":81");
        websocket.onopen=function(event){
          connected=true;
        };
        function resize(){
          canvas.width=window.innerWidth;
          canvas.height=window.innerHeight;
          radius=0.04*Math.sqrt(Math.pow(canvas.width,2)+Math.pow(canvas.height,2));
          draw();
        }
        window.addEventListener("resize",resize,false);
        resize();function draw(){
          context.fillStyle="#dddddd";
          context.fillRect(0,0,canvas.width,canvas.height);
          context.beginPath();
          context.arc(((x+1)/2)*(canvas.width-2*radius)+radius,
              ((-y+1)/2)*(canvas.height-2*radius)+radius,
              radius,0,2*Math.PI,false);
          context.fillStyle="#00dd00";
          context.fill();
          context.lineWidth=5;
          context.strokeStyle="#008800";
          context.stroke();
        }
        function updateCoordinates(mx,my){
          x=((mx-radius)/(canvas.width-2*radius))*2-1;
          y=-(((my-radius)/(canvas.height-2*radius))*2-1);
          if(x<-1.0) x=-1.0;
          else if(x>1.0) x=1.0;
          if(y<-1.0) y=-1.0;
          else if(y>1.0) y=1.0;
        }
        window.addEventListener("mousemove",mouseMove,false);
        window.addEventListener("mousemove",throttle(updateServer,minUpdateServerTime),false);
        function mouseMove(event){
          updateCoordinates(event.clientX,event.clientY);
          draw();
        }
        window.addEventListener("touchstart",touchEvent,false);
        window.addEventListener("touchstart",throttle(updateServer,minUpdateServerTime),false);
        window.addEventListener("touchmove",touchEvent,false);
        window.addEventListener("touchmove",throttle(updateServer,minUpdateServerTime),false);
        function touchEvent(event){
          var touches=event.touches;
          if(touches.length>0){
            var touch=touches.item(0);
            updateCoordinates(touch.clientX,touch.clientY);
          }
          draw();
        }
        window.addEventListener("touchend",touchReleaseEvent,false);
        window.addEventListener("touchend",throttle(updateServer,minUpdateServerTime),false);
        window.addEventListener("touchcancel",touchReleaseEvent,false);
        window.addEventListener("touchCancel",throttle(updateServer,minUpdateServerTime),false);
        function touchReleaseEvent(event){
          x=0.0,y=0.0;
          draw();
        }
        function updateServer(){
          if(connected){
            websocket.send(x.toFixed(3)+","+y.toFixed(3));
          }else{
            alert("non connecté");
          }
        }
        function throttle(func,wait){
          var timeout;
          return function(){
            if(!timeout){
              timeout=setTimeout(function(){
                timeout=null;
                func();
              }, wait);
            }
          }
        }
      }
      window.onload=setup;
    </script>
  </body>
</html>
);

#endif
